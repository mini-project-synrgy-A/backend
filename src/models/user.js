'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.hasOne(models.Profile)
      User.hasMany(models.Cart)
      User.hasMany(models.Review)
      User.hasMany(models.Favorite)
      User.hasMany(models.Order)
      User.belongsTo(models.Role)
      // define association here
    }
  }

  User.init(
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      RoleId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'User',
    }
  );

  return User;
};
