'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Review extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Review.belongsTo(models.Menu)
      Review.belongsTo(models.User)
      Review.belongsTo(models.Order)
      // define association here
    }
  };
  Review.init({
    MenuId: DataTypes.INTEGER,
    UserId: DataTypes.INTEGER,
    OrderId: DataTypes.INTEGER,
    rate: DataTypes.FLOAT,
    review: DataTypes.STRING,
    imageUrl: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Review',
  });
  return Review;
};