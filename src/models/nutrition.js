'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Nutrition extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Nutrition.belongsTo(models.Menu)
      // define association here
    }
  };
  Nutrition.init({
    MenuId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    value: DataTypes.INTEGER,
    unit: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Nutrition',
  });
  return Nutrition;
};