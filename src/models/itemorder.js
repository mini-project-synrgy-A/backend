'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ItemOrder extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ItemOrder.belongsTo(models.Order)
      ItemOrder.belongsTo(models.Menu)
    }
  };
  ItemOrder.init({
    OrderId: DataTypes.INTEGER,
    MenuId: DataTypes.INTEGER,
    price: DataTypes.INTEGER,
    quantity: DataTypes.INTEGER,
    deliveryDate: DataTypes.DATE,
    note: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ItemOrder',
  });
  return ItemOrder;
};