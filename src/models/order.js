'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Order.belongsTo(models.User)
      Order.belongsTo(models.Discount)
      Order.hasMany(models.ItemOrder)
      Order.hasMany(models.Review)
      // define association here
    }
  };
  Order.init({
    UserId: DataTypes.INTEGER,
    DiscountId: DataTypes.INTEGER,
    deliveryAddress: DataTypes.STRING,
    note: DataTypes.STRING,
    status: DataTypes.STRING,
    discountAmount: DataTypes.INTEGER,
    amountPaid: DataTypes.INTEGER,
    paidAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Order',
  });
  return Order;
};
