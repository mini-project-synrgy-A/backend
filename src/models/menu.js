'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Menu extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Menu.belongsTo(models.MenuCategory)
      Menu.hasMany(models.Favorite)
      Menu.hasMany(models.Nutrition)
      Menu.hasMany(models.Cart)
      Menu.hasMany(models.Review)
      Menu.hasMany(models.ItemOrder)
      Menu.hasMany(models.Tag)
      // define association here
    }
  };
  Menu.init({
    MenuCategoryId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    price: DataTypes.INTEGER,
    description: DataTypes.STRING,
    bestBefore: DataTypes.TEXT,
    imageUrl: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Menu',
  });
  return Menu;
};