'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ScheduleCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      ScheduleCategory.hasMany(models.MenuSchedule)
      // define association here
    }
  };
  ScheduleCategory.init({
    categoryName: DataTypes.STRING,
    startDeliveryTime: DataTypes.DATE,
    endDeliveryTime: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'ScheduleCategory',
  });
  return ScheduleCategory;
};