const multer = require('multer');
const multerS3 = require('multer-s3');
const s3 = require('../lib/s3');
require('dotenv').config();

const upload = (subfolder) => multer({
    storage: multerS3({
        s3: s3,
        bucket: process.env.S3_BUCKET,
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: function (req, file, cb) {
            // console.log(file);
            cb(null, `${subfolder}/${Date.now() + '-' + file.originalname}`);
        }
    })
})

module.exports = upload;