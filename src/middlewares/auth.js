const { responseForbidden, responseError } = require('../lib/response');
const Token = require('../lib/token');

/**
 *
 * @param {} headers ['authorization']
 * @returns boolean
 */
const hasHeaderAuthorization = (headers) => {
  return headers && headers['authorization'];
};

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns next or response failed
 */
const validateToken = (req, res, next) => {
  try {
    if (hasHeaderAuthorization(req.headers)) {
      Token.setToken = req.headers['authorization'];
      req.user = Token.decoded();

      return next();
    }

    responseForbidden(res, 'Headers authorization not found');
  } catch (error) {
    console.log(error);
    responseError(res, error.toString());
  }
};

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns next or response failed
 */
 const authAdmin = (req, res, next) => {
  try {
    if (hasHeaderAuthorization(req.headers)) {
      Token.setToken = req.headers['authorization'];
      req.user = Token.decoded();
      
      if (req.user.data.RoleId === 1) {
        return next();
      } else {
        responseForbidden(res, 'Not an admin');
      }
    }

    responseForbidden(res, 'Headers authorization not found');
  } catch (error) {
    console.log(error);
    responseError(res, error.toString());
  }
};

module.exports = {
  hasHeaderAuthorization,
  validateToken,
  authAdmin
};
