'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('Profiles', 'fullName', 'firstname');
    await queryInterface.addColumn('Profiles', 'lastname', {
      type: Sequelize.STRING,
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('Profiles', 'firstname', 'fullName');
    await queryInterface.changeColumn('Profiles', 'lastname', {
      type: Sequelize.STRING,
    });
  },
};
