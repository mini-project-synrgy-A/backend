'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('Users', 'role', 'RoleId');
    await queryInterface.changeColumn('Users', 'RoleId', {
      type: 'INTEGER USING CAST("RoleId" as INTEGER)',
    });
    await queryInterface.changeColumn('Users', 'username', {
      type: Sequelize.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Username address already in use!',
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('Users', 'RoleId', 'role');
    await queryInterface.changeColumn('Users', 'RoleId', {
      type: 'INTEGER USING CAST("RoleId" as INTEGER)',
    });
  },
};
