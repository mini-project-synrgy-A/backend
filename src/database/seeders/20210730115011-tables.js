'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', [{
      username: "team_a",
      password: "pass1234",
      RoleId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Profiles', [{
      UserId: 1,
      firstname: "team",
      lastname: "team",
      birthDate: "2000-02-02",
      occupation: "no role",
      address: "jkt",
      phone: "082133445566",
      email: "team.alpha@yahoo.com",
      imageUrl: "team.alpha.com",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('MenuCategories', [{
      categoryName: "pedas",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Nutrition', [{
      MenuId: 1,
      name: "karbohidrat",
      value: "1000",
      unit: "kkal",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Tags', [{
      MenuId: 1,
      name: "makanan modern",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Orders', [{
      UserId: 1,
      DiscountId: 1,
      deliveryAddress: "rumah jkt",
      note: "extra pedas",
      status: "done",
      discountAmount: 1000000,
      amountPaid: 1000000,
      paidAt: "2021-07-31",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Reviews', [{
      MenuId: 1,
      UserId: 1,
      OrderId: 1,
      rate: 4,
      review: "Good",
      imageUrl: "internet.com",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Carts', [{
      UserId: 1,
      MenuId: 1,
      price: 10000,
      quantity: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Menus', [{
      MenuCategoryId: 1,
      name: "capcai",
      price: 1000000,
      description: "ini capcai",
      bestBefore: "Menu ini disarankan untuk dimakan maksimal 6 jam setelah pemesanan",
      imageUrl: "capcai.com",
      status: "ready",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
 
    await queryInterface.bulkInsert('ItemOrders', [{
      OrderId: 1,
      MenuId: 1,
      price: 7000000,
      quantity: 10,
      deliveryDate: "2000-02-02",
      note: "setengah matang",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Discounts', [{
      name: "WIB",
      coupon: "abc",
      discountAmount: 10000,
      start: "2000-02-02",
      end: "2000-02-07",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Favorites', [{
      UserId: 1,
      MenuId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});


    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
    await queryInterface.bulkDelete('Profiles', null, {});
    await queryInterface.bulkDelete('MenuCategories', null, {});
    await queryInterface.bulkDelete('Nutrition', null, {});
    await queryInterface.bulkDelete('Tags', null, {});
    await queryInterface.bulkDelete('Orders', null, {});
    await queryInterface.bulkDelete('Reviews', null, {});
    await queryInterface.bulkDelete('Carts', null, {});
    await queryInterface.bulkDelete('Menus', null, {});
    await queryInterface.bulkDelete('ItemOrders', null, {});
    await queryInterface.bulkDelete('Discounts', null, {});
    await queryInterface.bulkDelete('Favorites', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
