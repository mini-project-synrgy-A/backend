function responseSuccess(res, data) {
  res.status(200).json({
    status: 'success',
    data,
  });
  return;
}

function responseCreated(res, data) {
  res.status(201).json({
    status: 'created',
    data,
  });
  return;
}

function responseAccepted(res, data) {
  res.status(202).json({
    status: 'accepted',
    data,
  });
  return;
}

function responseError(res, message) {
  res.status(500).json({
    status: 'error',
    message,
  });
  return;
}

function responseForbidden(res, message) {
  res.status(403).json({
    status: 'forbidden',
    message,
  });
  return;
}

function responseNotFound(res, message) {
  res.status(404).json({
    status: 'not found',
    message,
  });
  return;
}

module.exports = {
  responseAccepted,
  responseCreated,
  responseSuccess,
  responseError,
  responseForbidden,
  responseNotFound,
};
