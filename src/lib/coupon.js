function generateCoupon() {
    let coupon = "";
    let CHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for (let i = 0; i <= 6; i++) {
        coupon += CHAR.charAt(Math.floor(Math.random() * CHAR.length))
    }
    return coupon
}

module.exports = { generateCoupon }