const TagController = require('../../controllers/TagController');
const router = require('express').Router();
const { validateToken, authAdmin } = require('../../middlewares/auth');

// Get by menu id
router.get('/by-menu/:menuId', TagController.getByMenuId);

// Add by menu Id
router.post('/add', authAdmin, TagController.addByMenuId);

// Get detail by Id
router.get('/detail/:id', TagController.getById);

// Delete by Id
router.delete('/delete/:id', authAdmin, TagController.deleteById);

module.exports = router;