const router = require('express').Router();
const menuScheduleRouter = require('./menuScheduleRouter');
const nutritionRouter = require('./nutritionRouter');
const tagRouter = require('./tagRouter');
const { validateToken, authAdmin } = require('../../middlewares/auth');
const upload = require('../../middlewares/upload');

const MenuController = require('../../controllers/MenuController');

router.use('/schedule', menuScheduleRouter);
router.use('/nutrition', nutritionRouter);
router.use('/tag', tagRouter);

// All Menu
// Example: /all?min-price=10000&max-price=30000&category=["sunda"]&tags=["pedas","manis"]&schedule-category="siang"
router.get('/all', MenuController.getAll);

// Search
// Example: /search?keyword=value
router.get('/search', MenuController.search);

// Create
router.post('/create', authAdmin, upload('image/menu').single('image'), MenuController.create);

// Edit
router.patch('/edit/:id', authAdmin, upload('image/menu').single('image'), MenuController.edit);

// Delete
router.delete('/delete/:id', authAdmin, MenuController.delete);

// Detail
router.get('/detail/:id', MenuController.getById);

module.exports = router;