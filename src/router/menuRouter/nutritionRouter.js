const NutritionController = require('../../controllers/NutritionController');
const router = require('express').Router();
const { validateToken, authAdmin } = require('../../middlewares/auth');

// Get by menu id
router.get('/by-menu/:menuId', NutritionController.getByMenuId);

// Add by menu Id
router.post('/add', authAdmin, NutritionController.addByMenuId);

// Get detail by Id
router.get('/detail/:id', NutritionController.getById);

// Delete by Id
router.delete('/delete/:id', authAdmin, NutritionController.deleteById);

module.exports = router;