const MenuScheduleController = require('../../controllers/MenuScheduleController');
const router = require('express').Router();
const { validateToken, authAdmin } = require('../../middlewares/auth');

// Get by menu id
router.get('/by-menu/:menuId', MenuScheduleController.getByMenuId);

// Add by menu Id
router.post('/add', authAdmin, MenuScheduleController.addByMenuId);

// Get detail by Id
router.get('/detail/:id', MenuScheduleController.getById);

// Delete by Id
router.delete('/delete/:id', authAdmin, MenuScheduleController.deleteById);

module.exports = router;