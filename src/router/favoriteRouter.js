const router = require('express').Router();
const FavoriteController = require('../controllers/FavoriteController');
const { validateToken, authAdmin } = require('../middlewares/auth');

router.get('/all', validateToken, FavoriteController.getAll);

router.post('/add', validateToken, FavoriteController.add);

router.delete('/delete/:menuId', validateToken, FavoriteController.delete);


module.exports = router;
