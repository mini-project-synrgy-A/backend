const router = require('express').Router();

// Index (Get All)
router.get('/', (req, res) => {
    res.send('index user');
    // scheduleCategoryController::index
});

// Create
router.post('/create', (req, res) => {
    res.send("/create");
    // scheduleCategoryController::create
});

// Edit
router.patch('/edit/:id', (req, res) => {
    res.send(`edit: ${req.params.id}`);
    // scheduleCategoryController::edit
});

// Delete
router.delete('/delete/:id', (req, res) => {
    res.send(`delete: ${req.params.id}`);
    // scheduleCategoryController::delete
});

module.exports = router;