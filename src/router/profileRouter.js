const router = require('express').Router();
const ProfileController = require('../controllers/ProfileController');
const { validateToken, authAdmin } = require('../middlewares/auth');
const upload = require('../middlewares/upload');

const authRouter = () => {
  // get detail profile for customer
  router.get('/detail', validateToken, ProfileController.getProfile);

  // update profile
  router.put('/update', validateToken, upload('image/profile').single('image'), ProfileController.updateProfile);

  // delete profile
  router.delete('/:userId', authAdmin, ProfileController.destroy);

  // get all profiles
  router.get('/', authAdmin, ProfileController.getAll);

  return router;
};

module.exports = authRouter;
