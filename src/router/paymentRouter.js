const router = require('express').Router();

// Index (Get All)
router.get('/', (req, res) => {
    res.send('index user');
    // paymentController::index
})

// Get Order by User Id (Customer)
// Parameter id diambil dari dekripsi JWT
router.get('/getByUserId', (req, res) => {
    res.send('/getByUserId');
    // paymentController::getByUserIdCustomer
});

// Get Order by User Id (Admin)
router.get('/getByUserId/:id', (req, res) => {
    res.send(`/getByUserId/${req.params.id}`);
    // paymentController::getByUserIdAdmin
});

// Create
router.post('/create', (req, res) => {
    res.send("/create");
    // paymentController::create
});

// Edit
router.patch('/edit/:id', (req, res) => {
    res.send(`edit: ${req.params.id}`);
    // paymentController::edit
});

// Delete
router.delete('/delete/:id', (req, res) => {
    res.send(`delete: ${req.params.id}`);
    // paymentController::delete
});

module.exports = router;