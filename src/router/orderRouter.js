const router = require('express').Router();
const OrderController = require('../controllers/OrderController');
const { validateToken, authAdmin } = require('../middlewares/auth');

// Get all order (include filter & search)
// http://localhost:3000/order/all?status=canceled&isPaid=true&deliveryAddress=surabaya&createdAt=2021-07-31
router.get('/all', validateToken, OrderController.getAll);

// Get order by Id
router.get('/detail/:id', validateToken, OrderController.getById);

// Create
router.post('/create', validateToken, OrderController.createOrder);

// Cancel Order
router.patch('/cancel/:id', validateToken, OrderController.cancelOrder);

// Paid Order
router.patch('/paid/:id', authAdmin, OrderController.paidOrder);

// Done Order
router.patch('/done/:id', validateToken, OrderController.doneOrder);

// Edit
router.patch('/edit/:id', authAdmin, OrderController.editOrder);

// Delete
router.delete('/delete/:id', authAdmin, OrderController.destroyOrder);

module.exports = router;