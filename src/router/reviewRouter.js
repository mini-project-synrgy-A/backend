const router = require('express').Router();
const ReviewController = require('../controllers/ReviewController');
const { validateToken, authAdmin } = require('../middlewares/auth');
const upload = require('../middlewares/upload');

// Get all order (include filter & search)
router.get('/all', ReviewController.getAll);

// Get order by reviewId
router.get('/detail/:id', ReviewController.getById);

// Create
router.post('/create', validateToken, upload('image/review').single('image'), ReviewController.createReview);

// Delete
router.delete('/delete/:id', authAdmin, ReviewController.destroyReview);

module.exports = router;