const router = require('express').Router();
const userRouter = require('./userRouter');
const profileRouter = require('./profileRouter');
const menuCategoryRouter = require('./menuCategoryRouter');
const menuRouter = require('./menuRouter');
const scheduleCategoryRouter = require('./scheduleCategoryRouter');
const discountRouter = require('./discountRouter');
const orderRouter = require('./orderRouter');
const paymentRouter = require('./paymentRouter');
const reviewRouter = require('./reviewRouter');
const authRouter = require('./authRouter');
const favoriteRouter = require('./favoriteRouter');
const { responseSuccess } = require('../lib/response');
const cartRouter = require('./cartRouter');

router.use('/auth', authRouter());
router.use('/users', userRouter());
router.use('/profiles', profileRouter());
router.use('/carts', cartRouter());
router.use('/menu-category', menuCategoryRouter);
router.use('/menu', menuRouter);
router.use('/schedule-category/', scheduleCategoryRouter);
router.use('/discount', discountRouter);
router.use('/order', orderRouter);
router.use('/payment', paymentRouter);
router.use('/reviews', reviewRouter);
router.use('/favorite', favoriteRouter);
router.get('/', (req, res) => {
  responseSuccess(res, 'Hello World!');
});

module.exports = router;
