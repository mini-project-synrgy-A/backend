const router = require('express').Router();
const AuthController = require('../controllers/AuthController');
const UsersController = require('../controllers/UsersController');

const authRouter = () => {
  // login
  router.post('/login', AuthController.login);

  return router;
};

module.exports = authRouter;
