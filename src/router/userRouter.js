const router = require('express').Router();
const UsersController = require('../controllers/UsersController');
const { validateToken, authAdmin } = require('../middlewares/auth');

const userRouter = () => {
  //  #========== general ===========#
  // Register user
  router.post('/register', UsersController.createCustomer);

  // Get user by id (user token)
  router.get('/detail', validateToken, UsersController.getByUserId);

  // Check user token
  router.get('/check', validateToken, UsersController.getByUserId);

  //  #========== admin =============#
  // Register admin
  router.post('/register-admin', UsersController.createAdmin);

  // Delete user by id
  router.delete('/:userId', authAdmin, UsersController.destroy);

  // Get All User
  router.get('/', authAdmin, UsersController.getAll);

  return router;
};

module.exports = userRouter;
