const router = require('express').Router();
const CartController = require('../controllers/CartController');
const { validateToken, authAdmin } = require('../middlewares/auth');

const cartRouter = () => {
  //  #========== general ===========#
  // Register user
  router.post('/create', validateToken, CartController.createCart);

  // Get user by id (user token)
  router.get('/detail', validateToken, CartController.getCart);

  // Get user by id (user token)
  router.put('/update', validateToken, CartController.updateCart);

  // Delete user by id
  router.delete('/:cartId', validateToken, CartController.destroy);

  // delete all user cart
  router.delete('/', validateToken, CartController.destroyAllUserCart);
  
  //  #========== admin =============#

  // Get All User
  router.get('/', authAdmin, CartController.findAll);
  

  return router;
};

module.exports = cartRouter;
