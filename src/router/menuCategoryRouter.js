const router = require('express').Router();
const MenuCategoryController = require('../controllers/MenuCategoryController');

const { validateToken, authAdmin } = require('../middlewares/auth');

// Create
router.post('/create', authAdmin, MenuCategoryController.add)

// Delete
router.delete('/delete/:id', authAdmin, MenuCategoryController.delete);

// Get All
router.get('/all', MenuCategoryController.getAll);

// Get Menu by category
router.get('/:id', MenuCategoryController.getMenuByCategoryId);

module.exports = router