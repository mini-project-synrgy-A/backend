const router = require('express').Router();
const DiscountController = require('../controllers/DiscountController');
const { validateToken, authAdmin } = require('../middlewares/auth');

// Get all order (include filter & search)
// http://localhost:3000/order/all?status=canceled&isPaid=true&deliveryAddress=surabaya&createdAt=2021-07-31
router.get('/', DiscountController.getActiveDiscount);
router.get('/all', authAdmin, DiscountController.getAllDiscount); // admin

// Get order by Id
router.get('/detail/:id', DiscountController.getById);

// Create
router.post('/create', authAdmin, DiscountController.createDiscount);

// Edit
router.patch('/edit/:id', authAdmin, DiscountController.editDiscount);

// Delete
router.delete('/delete/:id', authAdmin, DiscountController.destroyDiscount);

module.exports = router;