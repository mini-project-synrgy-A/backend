const models = require('../models');
const { responseAccepted, responseCreated, responseError, responseForbidden, responseNotFound, responseSuccess } = require('../lib/response');


module.exports = {
    getByMenuId: async (req, res) => {
        try {
            const MenuId = req.params.menuId;

            const menu = await models.Menu.findOne({ where: { id: MenuId }});

            if (menu) {
                const nutritions = await models.Nutrition.findAll({ 
                    where: { 
                        MenuId 
                    }
                })
                responseSuccess(res, nutritions);
            } else {
                responseNotFound(res, "Menu not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    addByMenuId: async (req, res) => {
        try {
            const menu = await models.Menu.findOne({ where: { id: req.body.menu_id }});

            if (menu) {
                const {
                    menu_id,
                    name,
                    value,
                    unit
                } = req.body;
    
                const nutrition = await models.Nutrition.create({ 
                    MenuId: menu_id,
                    name,
                    value,
                    unit
                })
                responseSuccess(res, nutrition);
            } else {
                responseNotFound(res, "Menu not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    getById: async (req, res) => {
        try {
            const id = req.params.id;

            const nutrition = await models.Nutrition.findOne({ 
                where: { 
                    id 
                }
            })

            if (nutrition) {
                responseSuccess(res, nutrition);
            } else {
                responseNotFound(res, "Nutrition not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    deleteById: async (req, res) => {
        try {
            const id = req.params.id;

            const nutrition = await models.Nutrition.findOne({ 
                where: { 
                    id 
                }
            })

            if (nutrition) {
                models.Nutrition.destroy({ where: { id } });
                responseSuccess(res, { message: "Nutrition succesfully deleted" });
            } else {
                responseNotFound(res, "Nutrition not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    }
}