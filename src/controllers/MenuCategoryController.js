const models = require('../models');
const { responseAccepted, responseCreated, responseError, responseForbidden, responseNotFound, responseSuccess } = require('../lib/response');
const { response } = require('express');
const { Op } = require("sequelize");


module.exports = {
    getAll: async (req, res) => {
        try {
            const categories = await models.MenuCategory.findAll();
            responseSuccess(res, categories);
        } catch(err) {
            responseError(res, err.message);
        }
    },
    getMenuByCategoryId: async (req, res) => {
        try {
            const id = req.params.id;

            const menus = await models.Menu.findAll({
                include: [
                    models.Tag, 
                    models.MenuCategory, 
                    models.Nutrition, 
                ], 
                where: {
                    "$MenuCategory.id$": id
                }
            });

            responseSuccess(res, menus);
        } catch(err) {
            responseError(res, err.message);
        }
    },
    add: async (req, res) => {
        try {
            const {
                name
            } = req.body;

            const foundCategory = await models.MenuCategory.findOne({ where: { categoryName: name }});

            if (!foundCategory) {   
                const category = await models.MenuCategory.create({ 
                    categoryName: name
                })
                
                responseSuccess(res, category);
            } else {
                responseError(res, "Category already exist");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    delete: async (req, res) => {
        try {
            const id = req.params.id;

            const foundCategory = await models.MenuCategory.findOne({ 
                where: { 
                    id 
                }
            })

            if (foundCategory) {
                models.MenuCategory.destroy({ where: { id } });

                responseSuccess(res, { message: "Menu category succesfully deleted" });
            } else {
                responseNotFound(res, "Category not found");
            }
        } catch(err) {
            responseError(res, err.message)
        }
    }
}