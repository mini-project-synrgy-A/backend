const { User, Profile } = require('../models');
const Password = require('../lib/password');
const { responseCreated, responseError, responseSuccess } = require('../lib/response');
const DEFAULT_PROFILE_IMG = "https://synrgy-img.s3.ap-southeast-1.amazonaws.com/image/profile/default.jpg";

class UsersController {
  async getAll(req, res) {
    try {
      const data = await User.findAll();
      responseSuccess(res, data);
    } catch (error) {
      console.log('getAll:', error);
      return responseError(error.toString());
    }
  }

  async createCustomer(req, res) {
    try {
      const { username, password, firstname, lastname } = req.body;
      const findUser = await User.findOne({ where: { username } });

      if (findUser !== null) {
        return responseError(res, 'Sorry, username already taken');
      }

      if (!password || !username || !firstname || !lastname) {
        return responseError(res, 'Please check your data');
      }

      const userData = await User.create({
        username,
        password: await Password.makePassword(password),
        RoleId: 2,
      });

      if (userData === null) {
        return responseError(res, 'Failed created data');
      }

      await Profile.create({
        UserId: userData.id,
        firstname,
        lastname,
        imageUrl: DEFAULT_PROFILE_IMG
      });

      return responseCreated(res, 'Successfully created data');
    } catch (error) {
      console.log('createCustomer:', error);
      return responseError(error.toString());
    }
  }

  async createAdmin(req, res) {
    try {
      const { username, password } = req.body;
      const findUser = await User.findOne({ where: { username } });

      if (findUser !== null) {
        return responseError(res, 'Sorry, username already taken');
      }

      if (!password || !username) {
        return responseError(res, 'Please check your data');
      }

      await User.create({
        username,
        password: await Password.makePassword(password),
        RoleId: 1,
      });

      return responseCreated(res, 'Successfully created data');
    } catch (error) {
      console.log('createAdmin:', error);
      return responseError(error.toString());
    }
  }

  async destroy(req, res) {
    try {
      const id = req.params.userId;
      const response = await User.destroy({
        where: {
          id,
        },
      });

      if (response) {
        await Profile.destroy({
          where: {
            UserId: id,
          },
        });
      }

      responseSuccess(res, 'data has been deleted');
    } catch (error) {
      responseError(res, error.toString());
    }
  }

  async getByUserId(req, res) {
    try {
      const { data } = req.user;
      const userData = await User.findOne({
        where: {
          id: data.id,
        },
        attributes: {
          exclude: ['password'],
        },
      });

      responseSuccess(res, userData);
    } catch (error) {
      console.log('getByUserId:', error);
      responseError(res, error.toString());
    }
  }

  async checkToken(req, res) {
    try {
      responseSuccess(res, req.user);
    } catch(err) {
      responseError(res, error.toString());
    }
  }
}

module.exports = new UsersController();
