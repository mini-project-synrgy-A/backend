const models = require('../models');
const { responseAccepted, responseCreated, responseError, responseForbidden, responseNotFound, responseSuccess } = require('../lib/response');

module.exports = {
    getByMenuId: async (req, res) => {
        try {
            const MenuId = req.params.menuId;

            const menu = await models.Menu.findOne({ where: { id: MenuId }});

            if (menu) {
                const tags = await models.Tag.findAll({ 
                    where: { 
                        MenuId 
                    }
                })
                
                responseSuccess(res, tags);
            } else {
                responseNotFound(res, "Menu not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    addByMenuId: async (req, res) => {
        try {
            const menu = await models.Menu.findOne({ where: { id: req.body.menu_id }});

            if (menu) {
                const {
                    menu_id,
                    name
                } = req.body;
    
                const tag = await models.Tag.create({ 
                    MenuId: menu_id,
                    name
                })

                responseSuccess(res, tag);
            } else {
                responseNotFound(res, "Menu not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    getById: async (req, res) => {
        try {
            const id = req.params.id;

            const tag = await models.Tag.findOne({ 
                where: { 
                    id 
                }
            })

            if (tag) {
                responseSuccess(res, tag);
            } else {
                responseNotFound(res, "Tag not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    deleteById: async (req, res) => {
        try {
            const id = req.params.id;

            const tag = await models.Tag.findOne({ 
                where: { 
                    id 
                }
            })

            if (tag) {
                models.Tag.destroy({ where: { id } });

                responseSuccess(res, { message: "Tag succesfully deleted" });
            } else {
                responseNotFound(res, "Tag not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    }
}