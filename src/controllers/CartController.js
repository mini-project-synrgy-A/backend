const { responseSuccess, responseError } = require('../lib/response');
const { Cart, Menu } = require('../models');

class CartController {
  async findAll(req, res) {
    try {
      const cartData = await Cart.findAll({
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });

      responseSuccess(res, cartData);
    } catch (error) {
      console.log('findAll:', error);
      responseError(res, error.toString());
    }
  }

  async createCart(req, res) {
    try {
      const { data } = req.user;
      const { carts } = req.body;

      if (!carts) {
        if (carts.length === 0) {
          return responseError(res, 'Please check your data');
        }
        return responseError(res, 'Please check your data');
      }

      const cartData = carts.map((cart) => {
        return {
          ...cart,
          UserId: data.id,
        };
      });

      const currentCart = await Cart.findAll({
        where: {
          UserId: data.id,
        },
        attributes: ['id'],
      });

      if (currentCart.length > 0) {
        const deletedCart = [];
        currentCart.forEach((cart) => {
          deletedCart.push(cart.id);
        });
        Cart.destroy({ where: { id: deletedCart } });
      }

      const result = await Cart.bulkCreate(cartData);
      if (result === null) {
        return responseError(res, 'Failed created data');
      }

      responseSuccess(res, result);
    } catch (error) {
      console.log('getCart:', error);
      responseError(res, error.toString());
    }
  }

  async updateCart(req, res) {
    try {
      const { data } = req.user;
      const { carts } = req.body;

      if (!carts) {
        if (carts.length === 0) {
          return responseError(res, 'Please check your data');
        }
        return responseError(res, 'Please check your data');
      }

      const cartData = carts.map((cart) => {
        return {
          ...cart,
          UserId: data.id,
        };
      });

      const result = await Cart.bulkCreate(cartData, {
        fields: ['id', 'price', 'quantity', 'MenuId'],
        updateOnDuplicate: ['price', 'quantity', 'MenuId'],
      });

      if (result === null) {
        return responseError(res, 'Failed created data');
      }

      responseSuccess(res, 'Successfully updated');
    } catch (error) {
      console.log('getCart:', error);
      responseError(res, error.toString());
    }
  }

  async getCart(req, res) {
    try {
      const { data } = req.user;
      const cartData = await Cart.findAll({
        where: {
          UserId: data.id,
        },
        include: [
          {
            model: Menu,
            attributes: {
              exclude: ['createdAt', 'updatedAt'],
            },
          },
        ],
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'UserId'],
        },
      });

      responseSuccess(res, cartData);
    } catch (error) {
      console.log('getCart:', error);
      responseError(res, error.toString());
    }
  }

  async destroy(req, res) {
    try {
      const id = req.params.cartId;
      await Cart.destroy({
        where: {
          id,
        },
      });

      responseSuccess(res, 'data has been deleted');
    } catch (error) {
      responseError(res, error.toString());
    }
  }

  async destroyAllUserCart(req, res) {
    try {
      const { data } = req.user;
      await Cart.destroy({
        where: {
          UserId: data.id,
        },
      });

      responseSuccess(res, 'data has been deleted');
    } catch (error) {
      responseError(res, error.toString());
    }
  }
}

module.exports = new CartController();
