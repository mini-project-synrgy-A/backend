const { responseSuccess, responseError } = require('../lib/response');
const { User, Profile } = require('../models');
const DEFAULT_PROFILE_IMG = "https://synrgy-img.s3.ap-southeast-1.amazonaws.com/image/profile/default.jpg";
const BASE_URL_IMAGE = 'https://synrgy-img.s3.ap-southeast-1.amazonaws.com';

class ProfileController {
  async getAll(req, res) {
    try {
      const queryParams = require('url').parse(req.url, true).query;
      const query = {
        include: [
          {
            model: Profile,
            attributes: {
              exclude: ['createdAt', 'updatedAt'],
            },
          },
        ],
        attributes: {
          exclude: ['password'],
        },
      };

      if (queryParams.roleId) {
        query.where = {
          RoleId: queryParams.roleId,
        };
      }

      const userData = await User.findAll(query);

      responseSuccess(res, userData);
    } catch (error) {
      console.log('getProfile:', error);
      responseError(res, error.toString());
    }
  }

  async getProfile(req, res) {
    try {
      const { data } = req.user;
      const userData = await Profile.findOne({
        where: {
          UserId: data.id,
        },
        attributes: {
          exclude: ['UserId'],
        },
      });

      responseSuccess(res, userData);
    } catch (error) {
      console.log('getProfile:', error);
      responseError(res, error.toString());
    }
  }

  async updateProfile(req, res) {
    try {
      const { data } = req.user;
      const payload = req.body;
      const query = { where: { UserId: data.id } };
      const isIdAvailable = await Profile.findOne(query);

      if (isIdAvailable == null) {
        return responseError(res, 'Data Unavailable.');
      }

      if (!req.file || Object.keys(req.file).length === 0) {
        payload.imageUrl = isIdAvailable.imageUrl ? isIdAvailable.imageUrl : DEFAULT_PROFILE_IMG;
      } else {
        payload.imageUrl = `${BASE_URL_IMAGE}/${req.file.key}`;
      }

      const result = await Profile.update(payload, query);

      responseSuccess(res, 'Successfully updated data');
    } catch (error) {
      console.log('updateProfile:', error);
      responseError(error.toString());
    }
  }

  async destroy(req, res) {
    try {
      const id = req.params.userId;
      const response = await User.destroy({
        where: {
          id,
        },
      });

      if (response) {
        await Profile.destroy({
          where: {
            UserId: id,
          },
        });
      }

      responseSuccess(res, 'data has been deleted');
    } catch (error) {
      console.log('destroy:', error);
      responseError(res, error.toString());
    }
  }
}

module.exports = new ProfileController();
