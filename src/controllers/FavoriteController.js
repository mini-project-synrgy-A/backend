const { Favorite, Menu } = require('../models');
const { responseCreated, responseError, responseSuccess, responseNotFound } = require('../lib/response');

module.exports = {
    getAll: async (req, res) => {
        try {
            const UserId = req.user.data.id;
        
            const favorites = await Favorite.findAll({ 
                where: { UserId },
                include: [Menu] 
            });
            
            responseSuccess(res, favorites);
        } catch(err) {
            responseError(res, err.message);
        }
    },
    add: async (req, res) => {
        try {
            const UserId = req.user.data.id;
        
            const { menu_id: MenuId } = req.body;

            const favorite = await Favorite.findOne({ where: { UserId, MenuId } });
            
            if (!favorite) {
                const menu = await Menu.findOne({ where: { id: MenuId } });

                if (menu) {
                    const favorite = await Favorite.create({
                        UserId,
                        MenuId
                    });

                    responseCreated(res, favorite);
                } else {
                    responseNotFound(res, "Menu not found");
                }
            } else {
                responseError(res, "Menu already exist in user's favorite");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    delete: async (req, res) => {
        try {
            const MenuId = req.params.menuId;
            const UserId = req.user.data.id;

            const favorite = await Favorite.findOne({ where: { UserId, MenuId } });

            if (favorite) {
                await Favorite.destroy({ where: { UserId, MenuId } });

                responseSuccess(res, { message: "User's favorite successfully deleted" });
            } else {
                responseNotFound(res, "User's favorite menu not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    }
}