const models = require('../models');
const { Op } = require("sequelize");
const { responseCreated, responseError, responseNotFound, responseSuccess } = require('../lib/response');
const BASE_URL_IMAGE = 'https://synrgy-img.s3.ap-southeast-1.amazonaws.com';

module.exports = {
    create: async (req, res) => {
        try {
            let image_url, tagList, menuNutritions;

            if (!req.file || Object.keys(req.file).length === 0) {
                image_url = BASE_URL_IMAGE + '/image/menu/default.jpg';
            } else {
                image_url = `${BASE_URL_IMAGE}/${req.file.key}`;
            }
            
            const { 
                name,
                menu_category_id, // dropdown from table
                price,
                description,
                status, // dropdown string value ["ready". "not ready"]
                nutritions, // create new data
                tags,
                best_before
            } = req.body;
            
            const menuCategoryId = parseInt(menu_category_id);
            const menuPrice = parseInt(price);
            const bestBefore = best_before ? best_before : "";
            
            if (typeof(tags) === 'string')
                tagList = tags.split();
            else
                tagList = tags;
            
            if (typeof(nutritions) === 'string'){ 
                menuNutritions = nutritions.split();
            } else {
                menuNutritions = nutritions ? nutritions.map(nutrition => {
                    if (typeof(nutrition) !== 'object')
                        return JSON.parse(nutrition)
                    else
                        return nutrition
                }) : [];
            }

            const menu = await models.Menu.create({
                name,
                MenuCategoryId: menuCategoryId,
                price: menuPrice,
                description,
                imageUrl: image_url,
                status,
                bestBefore
            })
            
            menuNutritions.forEach(async (nutrition) => {
                await models.Nutrition.create({
                    MenuId: menu.id,
                    name: nutrition.name,
                    value: nutrition.value,
                    unit: nutrition.unit
                });
            })

            tagList.forEach(async (tag) => {
                await models.Tag.create({
                    MenuId: menu.id,
                    name: tag,
                })
            })

            responseCreated(res, menu);
        } catch(err) {
            responseError(res, err.message);
        }
    },
    getById: async (req, res) => {
        try {
            const id = req.params.id;

            const menu = await models.Menu.findOne({
                where: { id }, 
                include: [
                    models.Tag, 
                    models.MenuCategory, 
                    models.Nutrition, 
                ], 
            });

            if (menu) {
                responseSuccess(res, menu);
            } else {
                responseNotFound(res, "Menu not found");
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    getAll: async (req,res) => {
        try {
            const minPrice = req.query["min-price"];
            const maxPrice = req.query["max-price"];
            const userCategory = req.query["category"];
            
            let userTags = req.query["tags"] ? JSON.parse(req.query["tags"]) : [];

            if (typeof(userTags) === 'string')
                userTags = userTags.split();

            let menus = await models.Menu.findAll({ 
                include: [
                    models.Tag, 
                    models.MenuCategory, 
                    models.Nutrition, 
                ], 
            });
            
            menus = JSON.parse(JSON.stringify(menus, null, 2));
            menus = menus.filter(menu => {
                // Check filter for price
                let isInPriceGT = minPrice ? menu.price >= minPrice : true;
                let isInPriceLT = maxPrice ? menu.price <= maxPrice : true;

                // Check filter for tags
                let menuTags = menu.Tags.map(tag => tag.name);
                let isInTags = userTags ? userTags.every(tag => menuTags.includes(tag)) : true;

                // // Check filter for category
                let isInCategory = userCategory ? menu.MenuCategory.categoryName === userCategory : true;
                
                let isInFilter = isInPriceGT && isInPriceLT && isInTags && isInCategory;

                return isInFilter;
            });
            responseSuccess(res, menus);
        } catch(err) {
            responseError(res, err.message);

        }
    },
    edit: async (req,res) => {
        try {
            let image_url;

            const { 
                name,
                menu_category_id, // dropdown from table
                price,
                description,
                status, // dropdown string value ["ready". "not ready"]
                best_before
            } = req.body;

            const menuCategoryId = parseInt(menu_category_id);
            const menuPrice = parseInt(price);

            const foundMenu = await models.Menu.findOne({ where: { id: req.params.id } });
            if (foundMenu) {
                let attr = {};

                if (!req.file || Object.keys(req.file).length === 0) {
                    image_url = foundMenu.imageUrl;
                } else {
                    image_url = `${BASE_URL_IMAGE}/${req.file.key}`;
                }

                attr.imageUrl = image_url;
            
    
                if (name) 
                    attr.name = name;
                if (menuCategoryId)
                    attr.MenuCategoryId = menuCategoryId;
                if (menuPrice)
                    attr.price = menuPrice;
                if (description)
                    attr.description = description;
                if (status)
                    attr.status = status;
                if (best_before)
                    attr.bestBefore = best_before;
                
                const menu = await models.Menu.update(attr, { where: { id: req.params.id }, returning: true });
    
                responseSuccess(res, menu);
            } else {
                responseNotFound(res, "Menu not found")
            }
        } catch(err) {
            responseError(res, err.message);
        }
    },
    delete: async (req,res) => {
        try {
            query = {
                where: { id: req.params.id }
            }
            
            const menu = await models.Menu.findOne(query);
            
            
            if (menu) {
                await models.Tag.destroy({ where: { MenuId: req.params.id }});
                await models.Nutrition.destroy({ where: { MenuId: req.params.id }});
                await models.Menu.destroy(query);

                responseSuccess(res, { message: "Menu succesfully deleted" });
            } else {
                responseNotFound(res, "Menu not found");
            }   
        } catch(err) {
            responseError(res, err.message);
        }
    },
    search: async (req, res) => {
        try {
            const keyword = req.query.keyword;

            const menus = await models.Menu.findAll({
                where: {
                    name: {
                        [Op.iLike]: `%${keyword}%`
                    }
                }
            })

            responseSuccess(res, menus);
        } catch(err) {
            responseError(res, err.message);
        }
    }
} 