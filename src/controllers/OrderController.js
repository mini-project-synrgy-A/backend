const { Discount, Order, ItemOrder, User, Profile, Menu } = require('../models')
const { Op } = require("sequelize");
const { responseCreated, responseError, responseSuccess } = require('../lib/response');

function parseJson(items) {
    return (items.constructor === Array) ? items.map(item => JSON.parse(item)) : [JSON.parse(items)];
}

class OrderController {
    async getAll(req, res) {
        try {
            const userLogin = req.user.data;
            const where = req.query;
            let query = {};
            // search by id
            // if(where.id) {
            //     query.id = { [Op.eq]: where.id }
            // }
            // search by UserId
            if(where.UserId) {
                query.UserId = { [Op.eq]: where.UserId }
            }
            // search by deliveryAddress
            if(where.deliveryAddress) {
                query.deliveryAddress = { [Op.iLike]: `%${where.deliveryAddress}%` }
            }
            // filter orderDate
            if(where.createdAt) {
                const createdAt = new Date(where.createdAt);
                query.createdAt = { 
                    [Op.lt]: createdAt.getDate() + 1, 
                    [Op.gte]: createdAt
                }
            }
            // filter deliveryDate
            if(where.minDeliveryDate) {
                const minDeliveryDate = new Date(where.minDeliveryDate);
                query.deliveryDate = { [Op.gte]: minDeliveryDate }
            }
            if(where.maxDeliveryDate) {
                const maxDeliveryDate = new Date(where.maxDeliveryDate);
                query.deliveryDate = { [Op.lt]: maxDeliveryDate.getDate() + 1 }
            }
            // filter status
            if(where.status) {
                query.status = { [Op.eq]: where.status }
            }
            // filter isPaid
            if(where.isPaid == 'false') {
                query.paidAt = { [Op.eq]: null }
            } else if (where.isPaid == 'true') {
                query.paidAt = { [Op.not]: null }
            }
            // check all order by userId
            if(userLogin.RoleId !== 1) {
                query.UserId = userLogin.id
            }
            const orders = await Order.findAll({
                include: [{
                    model: User,
                    include: [{
                        model: Profile,
                        attributes: ['firstname', 'lastname'],
                    }],
                    attributes: ['username']
                  }],
                where: query
            })
            if (orders == null || orders == '') return responseError(res, 'Data Unavailable.');

            responseSuccess(res, orders);
        } catch (error) {
            console.log('getAll:', error);
            responseError(res, error.toString());
        }
    };
    async getById(req, res) {
        try {
            const orderId = req.params.id;
            let query = {};
            const userLogin = req.user.data;
            // check all order by userId
            if(userLogin.RoleId !== 1) {
                query.UserId = userLogin.id
            }
            query.id = orderId;
            
            const orders = await Order.findOne({
                include: [{
                    model: ItemOrder,
                    include: [{
                        model: Menu,
                        attributes: ['name', 'price', 'imageUrl']
                    }],
                    attributes: {
                        exclude: ['createdAt', 'updatedAt']
                    }
                  }],
                  where: query,
                  attributes: {
                      exclude: ['createdAt', 'updatedAt']
                  }
            });
            if(orders == null) return responseError(res, 'Data Unavailable.');
            
            responseSuccess(res, orders);
        } catch (error) {
            console.log('getById:', error);
            responseError(res, error.toString());
        }
    };
    async createOrder(req, res) {
        try {
            const userLogin = req.user.data;
            const data = req.body;

            if (data.items == null) return responseError(res, "Order at least contains an item order.");
            const itemOrder = parseJson(data.items);

            // discount
            let DiscountId = null;
            let discountAmount = null;
            if(data.coupon) {
                const today = new Date();
                today.setHours(0);
                today.setMinutes(0);
                today.setMilliseconds(0);
                const isDiscount = await Discount.findOne({
                    where: {
                        coupon : { [Op.eq]: data.coupon },
                        start : { [Op.lt]: today },
                        end : { [Op.gte]: today }
                    }
                });
                if (isDiscount == null) {
                    return responseError(res, "Coupon invalid.")
                }
                DiscountId = isDiscount.id;
                discountAmount = isDiscount.discountAmount;
            }

            let totalPrice = 0;
            itemOrder.forEach(item => {
                totalPrice += item.price * item.quantity
            })

            const order = await Order.create({
                UserId: userLogin.id,
                DiscountId: DiscountId,
                deliveryAddress: data.deliveryAddress,
                note: data.note,
                status: "process",
                discountAmount: discountAmount,
                amountPaid: totalPrice - discountAmount
            });
            itemOrder.forEach(async (item) => {
                await ItemOrder.create({
                    OrderId: order.id,
                    MenuId: item.MenuId,
                    price: item.price,
                    quantity: item.quantity,
                    deliveryDate: item.deliveryDate,
                    note: item.note,
                })
            });

            responseCreated(res, 'Successfully created data');
        } catch (error) {
            console.log('createOrder:', error);
            responseError(res, error.toString());
        }
    };
    async editOrder(req, res) {
        try {
            const orderId = req.params.id;
            const data = req.body;
            const query = { where: { id: orderId } };

            const isIdAvailable = Order.findOne(query);
            if(isIdAvailable == null) return responseError(res, 'Data Unavailable.');
            
            if (data.items == null) return responseError(res, "Order at least contains an item order.");
            const itemOrder = parseJson(data.items);

            const order = await Order.update({
                deliveryAddress: data.deliveryAddress,
                note: data.note,
                status: data.status,
                paidAt: (!data.paidAt) ? null:data.paidAt
            }, query);
            itemOrder.forEach(async (item) => {
                await ItemOrder.update({
                    deliveryDate: item.deliveryDate,
                    note: (!item.note) ? null:item.note
                }, {
                    where: { id: item.id }
                })
            });

            responseSuccess(res, data);
        } catch (error) {
            console.log('editOrder:', error);
            responseError(res, error.toString());
        }
    };
    async cancelOrder(req, res) {
        try {
            const orderId = req.params.id;
            const query = {};
            query.id = { [Op.eq]: orderId }

            const isIdAvailable = Order.findOne(query);
            if(isIdAvailable == null) return responseError(res, 'Data Unavailable.');
            query.paidAt = { [Op.eq]: null };
            query.status = { [Op.eq]: "process" };

            const order = await Order.update({
                status: "canceled",
            }, {
                where: query
            });

            responseSuccess(res, "Order canceled.");
        } catch (error) {
            console.log('cancelOrder:', error);
            responseError(res, error.toString());
        }
    };
    async paidOrder(req, res) {
        try {
            const orderId = req.params.id;
            const query = {};
            query.id = { [Op.eq]: orderId };

            const isIdAvailable = Order.findOne(query);
            if(isIdAvailable == null) return responseError(res, 'Data Unavailable.');
            query.paidAt = { [Op.eq]: null };

            const order = await Order.update({
                paidAt: new Date(),
            }, {
                where: query
            });

            responseSuccess(res, "Payment received.");
        } catch (error) {
            console.log('paidOrder:', error);
            responseError(res, error.toString());
        }
    };
    async doneOrder(req, res) {
        try {
            const orderId = req.params.id;
            const query = {};
            query.id = { [Op.eq]: orderId };

            const isIdAvailable = Order.findOne(query);
            if(isIdAvailable == null) return responseError(res, 'Data Unavailable.');
            query.paidAt = { [Op.not]: null };

            const order = await Order.update({
                status: "done",
            }, {
                where: query
            });

            responseSuccess(res, "Order finished.");
        } catch (error) {
            console.log('doneOrder:', error);
            responseError(res, error.toString());
        }
    };
    async destroyOrder(req, res) {
        try {
            const orderId = req.params.id;
            const query = { where: { id: orderId } };

            const isIdAvailable = Order.findOne(query);
            if(isIdAvailable == null) return responseError(res, 'Data Unavailable.');

            const order = await Order.destroy(query);
            if(order) {
                await ItemOrder.destroy({
                    where: { OrderId: orderId }
                })
            }
            responseSuccess(res, 'data has been deleted');
        } catch (error) {
            console.log('destroyOrder:', error);
            responseError(res, error.toString());
        }
    };
}

module.exports = new OrderController();
