const { Review } = require('../models');
const fs = require('fs');
const { Op } = require("sequelize");
const { responseCreated, responseError, responseSuccess } = require('../lib/response');

class ReviewController {
    async getAll(req, res) {
        try {
            const where = req.query;
            let query = {};
            // search by rate
            if(where.rate) {
                query.rate = { [Op.eq]: parseFloat(where.rate) }
            }
            // search by MenuId
            if(where.MenuId) {
                query.MenuId = { [Op.eq]: where.MenuId }
            }
            // search by UserId
            if(where.UserId) {
                query.UserId = { [Op.eq]: where.UserId }
            }
            // search by UserId
            if(where.OrderId) {
                query.OrderId = { [Op.eq]: where.OrderId }
            }
            const reviews = await Review.findAll({
                where: query
            })
            if (reviews == null || reviews == '') {
                return responseError(res, 'Data Unavailable.')
            }

            responseSuccess(res, reviews);
        } catch (error) {
            console.log('getAll:', error);
            responseError(res, error.toString());
        }
    };
    async getById(req, res) {
        try {
            const reviewId = req.params.id;
            let query = {};
            query.id = reviewId;
            
            const review = await Review.findOne({
                where: query
            });
            if(review == null) {
                return responseError(res, 'Data Unavailable.')
            }
            
            responseSuccess(res, review);
        } catch (error) {
            console.log('getById:', error);
            responseError(res, error.toString());
        }
    };
    async createReview(req, res) {
        try {
            const userLogin = req.user.data;
            const data = req.body;
            let image_url;

            if (!req.file || Object.keys(req.file).length === 0) {
                image_url = 'https://synrgy-img.s3.ap-southeast-1.amazonaws.com/image/review/default.jpg';
            } else {
                image_url = `https://synrgy-img.s3.ap-southeast-1.amazonaws.com/${req.file.key}`;
            }

            // if (!req.files || Object.keys(req.files).length === 0) {
            //     image_url = '/image/review/default.jpg';
            // } else {
            //     // Receive image from user
            //     let image = req.files.image;
            //     image_url = `/image/review/${Date.now()}-${req.files.image.name}`;

            //     let imgPath = process.cwd() + '/public' + image_url;

            //     image.mv(imgPath, (err) => {
            //         if (err)
            //             return res.status(500).send(err);
            //     });
            // }

            const review = await Review.create({
                MenuId: data.MenuId,
                UserId: userLogin.id,
                OrderId: data.OrderId,
                rate: parseFloat(data.rate),
                review: data.review,
                imageUrl: image_url
            });

            responseCreated(res, 'Successfully created data');
        } catch (error) {
            console.log('createReview:', error);
            responseError(res, error.toString());
        }
    };
    async destroyReview(req, res) {
        try {
            const reviewId = req.params.id;
            const query = { where: { id: reviewId } };
            
            const isIdAvailable = await Review.findOne(query);
            if(isIdAvailable == null) {
                return responseError(res, 'Data Unavailable.')
            }

            const review = await Review.destroy(query);
            
            responseSuccess(res, 'data has been deleted');
        } catch (error) {
            console.log('destroyReview:', error);
            responseError(res, error.toString());
        }
    };
}

module.exports = new ReviewController();