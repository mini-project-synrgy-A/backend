const { Discount } = require('../models');
const { Op } = require("sequelize");
const { generateCoupon } = require("../lib/coupon");
const { responseCreated, responseError, responseSuccess } = require('../lib/response');

class DiscountController {
    async getActiveDiscount(req, res) {
        try {
            const where = req.query;
            let query = {};
            // search by coupon
            if(where.coupon) {
                query.coupon = { [Op.eq]: where.coupon }
            }
            // search by discount name
            if(where.name) {
                query.name = { [Op.iLike]: `%${where.name}%` }
            }
            // filter discount start valid date
            if(where.start) {
                const start = new Date(where.start);
                query.start = { [Op.gte]: start }
            }
            // filter discount end valid date
            if(where.end) {
                const end = new Date(where.end);
                query.end = { [Op.lt]: end.getDate() + 1 }
            }
            // get active discount
            const today = new Date();
            today.setHours(0);
            today.setMinutes(0);
            today.setMilliseconds(0);
            query.start = { [Op.lt]: today }
            query.end = { [Op.gte]: today }

            const discounts = await Discount.findAll({
                where: query
            })
            if (discounts == null || discounts == '') {
                return responseError(res, 'Data Unavailable.')
            }

            responseSuccess(res, discounts);
        } catch (error) {
            console.log('getAll:', error);
            responseError(error.toString());
        }
    };
    async getAllDiscount(req, res) {
        try {
            const where = req.query;
            let query = {};
            // search by coupon
            if(where.coupon) {
                query.coupon = { [Op.eq]: where.coupon }
            }
            // search by discount name
            if(where.name) {
                query.name = { [Op.iLike]: `%${where.name}%` }
            }
            // filter discount start valid date
            if(where.start) {
                const start = new Date(where.start);
                query.start = { [Op.gte]: start }
            }
            // filter discount end valid date
            if(where.end) {
                const end = new Date(where.end);
                query.end = { [Op.lt]: end.getDate() + 1 }
            }

            const discounts = await Discount.findAll({
                where: query
            })
            if (discounts == null || discounts == '') {
                return responseError(res, 'Data Unavailable.')
            }

            responseSuccess(res, discounts);
        } catch (error) {
            console.log('getAll:', error);
            responseError(error.toString());
        }
    };
    async getById(req, res) {
        try {
            const discountId = req.params.id;
            let query = {};
            query.id = discountId;
            
            const discount = await Discount.findOne({
                where: query
            });
            if(discount == null) {
                return responseError(res, 'Data Unavailable.')
            }
            
            responseSuccess(res, discount);
        } catch (error) {
            console.log('getById:', error);
            responseError(error.toString());
        }
    };
    async createDiscount(req, res) {
        try {
            const data = req.body;

            const discount = await Discount.create({
                name: data.name,
                coupon: generateCoupon(),
                discountAmount: data.discountAmount,
                start: data.start,
                end: data.end,
            });

            responseCreated(res, 'Successfully created data');
        } catch (error) {
            console.log('createDiscount:', error);
            responseError(error.toString());
        }
    };
    async editDiscount(req, res) {
        try {
            const discountId = req.params.id;
            const data = req.body;
            const query = { where: { id: discountId } };
            
            const isIdAvailable = await Discount.findOne(query);
            if(isIdAvailable == null) {
                return responseError(res, 'Data Unavailable.')
            }

            const discount = await Discount.update({
                name: data.name,
                discountAmount: data.discountAmount,
                start: data.start,
                end: data.end,
            }, query);

            responseSuccess(res, data);
        } catch (error) {
            console.log('editDiscount:', error);
            responseError(error.toString());
        }
    };
    async destroyDiscount(req, res) {
        try {
            const discountId = req.params.id;
            const query = { where: { id: discountId } };
            
            const isIdAvailable = await Discount.findOne(query);
            if(isIdAvailable == null) {
                return responseError(res, 'Data Unavailable.')
            }

            const discount = await Discount.destroy(query);
            
            responseSuccess(res, 'data has been deleted');
        } catch (error) {
            console.log('destroyDiscount:', error);
            responseError(error.toString());
        }
    };
}

module.exports = new DiscountController();