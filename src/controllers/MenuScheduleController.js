const models = require('../models');

module.exports = {
    getByMenuId: async (req, res) => {
        try {
            const MenuId = req.params.menuId;

            const menu = await models.Menu.findOne({ where: { id: MenuId }});
            if (menu) {
                const menuSchedules = await models.MenuSchedule.findAll({ 
                    where: { 
                        MenuId 
                    }
                })
    
                res.status(200).json({
                    status: 200,
                    message: "Succesfully fetched menu schedules",
                    data: menuSchedules
                })
            } else {
                res.status(404).json({
                    status: 404,
                    message: "Menu not found"
                })
            }
        } catch(err) {
            res.status(500).json({
                status: 500,
                message: err.message
            })
        }
    },
    addByMenuId: async (req, res) => {
        try {
            const menu = await models.Menu.findOne({ where: { id: req.body.menu_id }});

            if (menu) {
                const {
                    menu_id,
                    day,
                    schedule_category_id
                } = req.body;
    
                const menuSchedule = await models.MenuSchedule.create({ 
                    MenuId: menu_id,
                    ScheduleCategoryId: schedule_category_id,
                    day
                })
    
                res.status(200).json({
                    status: 200,
                    message: "Succesfully add menu schedule",
                    data: menuSchedule
                })
            } else {
                res.status(404).json({
                    status: 404,
                    message: "Menu not found"
                })
            }
        } catch(err) {
            res.status(500).json({
                status: 500,
                message: err.message
            })
        }
    },
    getById: async (req, res) => {
        try {
            const id = req.params.id;

            const menuSchedule = await models.MenuSchedule.findOne({ 
                where: { 
                    id 
                }
            })

            if (menuSchedule) {
                res.status(200).json({
                    status: 200,
                    message: "Succesfully fetched menu schedule",
                    data: menuSchedule
                })
            } else {
                res.status(404).json({
                    status: 404,
                    message: "Menu schedule not found"
                })
            }
        } catch(err) {
            res.status(500).json({
                status: 500,
                message: err.message
            })
        }
    },
    deleteById: async (req, res) => {
        try {
            const id = req.params.id;

            const menuSchedule = await models.MenuSchedule.findOne({ 
                where: { 
                    id 
                }
            })

            if (menuSchedule) {
                models.MenuSchedule.destroy({ where: { id } });

                res.status(200).json({
                    status: 200,
                    message: "Menu schedule succesfully deleted"
                });
            } else {
                res.status(404).json({
                    status: 404,
                    message: "Menu schedule not found"
                });
            }
        } catch(err) {
            res.status(500).json({
                status: 500,
                message: err.message
            })
        }
    }
}