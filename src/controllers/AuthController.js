const { User } = require('../models');
const Password = require('../lib/password');
const { responseSuccess, responseForbidden, responseError } = require('../lib/response');
const Token = require('../lib/token');

class AuthController {
  async login(req, res) {
    try {
      const { username, password } = req.body;
      const data = await User.findOne({
        where: {
          username,
        },
      });

      if (data) {
        if (await Password.matchPassword(password, data.password)) {
          const token = Token.sign({ data });
          responseSuccess(res, { token });
        } else {
          responseForbidden(res, 'Username and Password are wrong');
        }
      } else {
        responseForbidden(res, 'Username and Password are wrong');
      }
    } catch (error) {
      console.log('login:', error);
      responseError(res, 'Failed Authenticated');
    }
  }
}

module.exports = new AuthController();
